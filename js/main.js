
$(document).ready(function(){
    $(window).on('resize',function(){
    widthCheck()
    checkBoxes()
    checkItem()
    })
    checkBoxes()
    checkItem()
    function widthCheck(){
        if($(window).width() > 1024){
            $('.mobile').hide();
        }
    }
    function checkBoxes(){
        var tHeight = 0;
        $('.evlogo .box').each(function(){
            var dis = $(this);
            if (dis.outerHeight() > tHeight){
                height = dis
                tHeight = dis.outerHeight();
            }
        })
        $('.evlogo .box').height(tHeight);
    }
    function checkItem(){
        var tHeight = 0;
        $('.itemBox .list .itemList').each(function(){
            var dis = $(this);
            if (dis.outerHeight() > tHeight){
                tHeight = dis.outerHeight();
            }
        })
        $('.itemBox .list .itemList').height(tHeight);
    }
    // $('#footerWrap .sns-wrap li .btn-down').on('click',function(){
    //     window.location = './file/SV_Times_04_sk.docx'
    // })
    $('.burger').on('click',function(){
        $('.mobile').slideToggle().toggleClass('open');
        console.log('w');
    })

    $('.goTop').click(function(){
        $('html,body').stop().animate({scrollTop:0},1000);
    });
    $(document).on("click","#footerWrap .site-notice li:first-child a",function(){
        $("#popup").show();
    })
    $(document).on("click","#footerWrap .newsletter",function(){
        $("#subscribe").show();
    })
    $(document).on("click",".modal-box .btn-modal-close",function(){
        $("#subscribe .modal-box.active").removeClass("active");
        $("#subscribe .modal-box#first").addClass("active");
        $(".modal-bg").hide();
    })
    $(document).on("click",".modal-box a",function(){
        $(this).parents(".modal-box").removeClass("active").next().next().addClass("active");
    })
    /*$(document).on("click",".modal-bg .modal-box:last-child .btn-subscribe",function(){
        if($(this).parents(".modal-box").find("input").val() != ""){
         $(this).parents(".modal-box").removeClass("active").prev().addClass("active");   
        }
    })*/
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 150) {
            $('.goTop').addClass('active');
        }
        else {
            $('.goTop').removeClass('active');
        }
    }
})

var snsTitle = document.title;
var snsUrl = location.href;

//alert(snsUrl + '|' + snsTitle);

Kakao.init('1cf4885573510d2b8cfa39ebe13a8a55');
function sharedsns_kakaost() {
    Kakao.Story.share({
      url: snsUrl,
      text: snsTitle
    });
}


function sharedsns(sns) {
    var o;
    var _url = encodeURIComponent(snsUrl);
    var _title = encodeURIComponent(snsTitle);
    var _br  = encodeURIComponent('\r\n');

    switch(sns)
    {
        case 'facebook':
            window.open("http://www.facebook.com/sharer.php?u="+ _url +"&t="+ _title, "facebook_sharer","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=640,height=480");
            break;
        case 'twitter':
            //window.open("http://twitter.com/home?status="+ _url +"&t="+ _title, "twitter_sharer","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=640,height=480");
            window.open("http://twitter.com/intent/tweet?text="+ _title +"&url="+ _url, "twitter_sharer","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=640,height=480");
            break;
        case 'googleplus':
            window.open("https://plus.google.com/share?url="+ _url +"&t="+ _title, "googleplus_sharer","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=640,height=480");
            break;
        case 'kakaostory':
            sharedsns_kakaost();
            //window.open("https://story.kakao.com/share?url="+ _url +"&t="+ _title, "kakaostory_sharer","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=640,height=480");
            break;
        case 'band':
            var sharedtxt = _title + ' | ' + _url;
            //window.open("http://www.band.us/plugin/share?body="+ _title + "&route=" + _url, "share_band","width=410, height=540, resizable=no");
            window.open("http://www.band.us/plugin/share?body="+ sharedtxt, "share_band","width=410, height=540, resizable=no");

            break;
        case 'line':
            //window.open("http://line.me/R/msg/text/?"+ _title +"%0D%0A"+ _url, "share_line","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=640,height=480");
            window.open("http://line.naver.jp/R/msg/text/?"+ _title +"%0D%0A"+ _url, "share_line","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=640,height=480");
            break;
        case 'blog':
        window.open("https://share.naver.com/web/shareView.nhn?url="+_url+"&title="+_title,"share_blog","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=640,height=480");
    }
}


/*function newsletter_subscribe_win() {
    openPopup('apply');
    $("#mailChangeAgree").css("display","none");
    $("#emailFind").css("display","none");
    $('.popup-newsletter.popup-wrap > div').removeClass('active').eq(0).addClass('active');
    //window.open("http://www.skhappiness.org/webzine/subscription.do", "newsletter_subscribe_popup","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=450,height=380");
}*/


//공통함수
function fn_prev_site(){
    var trgtSite = $('#prev_site').val();
    if(trgtSite != ''){
        window.open('http://' + location.hostname + ':'+ location.port + '/webzine/'+trgtSite+'/', '_blank');
    }else{
        alert('지난호를 선택해주십시오.');        
        $("#prev_site option:eq(0)").prop("selected", true);
        
        return;
    }
}

function fn_prev_site_m(){
    var trgtSite = $('#prev_site_m').val();
    if(trgtSite != ''){
        window.open('http://' + location.hostname + ':' + location.port +'/webzine/'+trgtSite+'/', '_blank');
    }else{
        alert('지난호를 선택해주십시오.');
        $("#prev_site_m option:eq(0)").prop("selected", true);
        return;
    }
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
$(document).ready(function(){
    var changeFlag = getParameterByName('changeFlag');
    if( changeFlag == "Y" ) {
        $(".modal-bg#subscribe .modal-box#first").removeClass("active").next().next().addClass('active');
        $(".modal-bg#subscribe").show()
    }
});

function goReg() {
    if (window.confirm("구독신청 하시겠습니까?")) {
        var frm = document.inFrm;
        var email = frm.email.value;
        
        if (email == "") {
            alert("e-mail을 입력해주세요");
            frm.email.focus();
            return;
        }
        $("#memCode").val('0');
        $("#memCode2").val('0');
        $("#memCode3").val('0');
        $("#subyn").val('');
        $.ajax({
            url : "/webzine/subscriber/insert.do",
            type : "POST",
            dataType : "xml",
            data : $("#inFrm").serialize(),
            timeout : 1000 * 5,
            error : function(){    },
            success : function(data, status){
                var status = $(data).find("status").text();
                var msg = $(data).find("msg").text();
                alert(msg);
                if((parseInt(status) > 0)){
                    $("#email").val('');
                    $('.btn-modal-close').trigger("click")
                }
            }
        });
    }
}

function findEmail(){
    var frm = document.inFrm;
    var email = frm.find_email.value;
  
    if (email == "") {
        alert("e-mail을 입력해주세요.");
        frm.find_email.focus();
        return;
    }
    $("#email").val(email);
    $("#memCode").val('');
    $("#memCode2").val('');
    $("#memCode3").val('');
    $.ajax({
        url : "/webzine/subscriber/check.do",
        type : "POST",
        dataType : "xml",
        data : $("#inFrm").serialize(),
        timeout : 1000 * 5,
        error : function(){    },
        success : function(data, status){
            var status = $(data).find("status").text();
            var msg = $(data).find("msg").text();
            var subYn = $(data).find("subYn").text();
            if((parseInt(status) > 0)){
                $("#email").val(msg);
                $('.btn-modal-close').trigger("click");
                if(subYn == "Y") {
                    $("#mailY").attr('checked', true) ;
                    $("#mailN").attr('checked', false) ;
                } else {
                    $("#mailY").attr('checked', false) ;
                    $("#mailN").attr('checked', true) ;
                }
              $('#subscribe').show();
              $('#subscribe div').removeClass("active");
              $("#change").addClass("active");
                frm.find_email.value = "";
                console.log("sdf")
            }else{
                alert(msg);
            }

        }
    });

}

function mailChangeAgree(){
    var frm = document.inFrm;
    var email = frm.email.value;
    var choiceAgree = "";
    if(frm.mailChangeAgree1.checked == false && frm.mailChangeAgree2.checked == false){
        alert("수신 여부를 선택해 주세요.");
        frm.mailChangeAgree1.focus();
        return;
    }
    if(frm.mailChangeAgree1.checked == true) {
        choiceAgree = frm.mailChangeAgree1.value;
    }
    if(frm.mailChangeAgree2.checked == true) {
        choiceAgree = frm.mailChangeAgree2.value;
    }
    $("#subsyn").val(choiceAgree);

    $.ajax({
        url: "/webzine/subscriber/update.do",
        type : "POST",
        dataType : "xml",
        data : $("#inFrm").serialize(),
        timeout: 1000 * 5,
        error:function(request,status,error){
            alert("오류가 발생했습니다. 잠시 후 다시 시도해 주세요.");
        },
        success: function(xmlData, status)    {
            var state = $(xmlData).find("state").text();
            var msg = $(xmlData).find("msg").text();

                alert(msg);
            if (state == "1") {
                $("#email").val('');
                $('.btn-modal-close').trigger("click")
            }
        }
    });
}

function changeAgree (val) {
    if(val == 1) {
        $("#mailY").attr('checked', true) ;
        $("#mailN").attr('checked', false) ;
    } else {
        $("#mailY").attr('checked', false) ;
        $("#mailN").attr('checked', true) ;
    }
}